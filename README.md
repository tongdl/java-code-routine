
### 一主一从
主表一条数据，对应从表一条数据

举例：查询一条商品信息，其中分类名来源于分类表，返回字段如下

* goods_id   商品id，来源于商品表
* goods_name 商品名，来源于商品表
* cate_id    分类id，来源于商品表
* cate_name  分类名，来源于分类表
* price      商品金额，来源于商品表

查看：com.itheima.service.MyBatisPlusService.demo003(String goodsId);

代码公式：

```java
// 步骤一：selectOne查询商品表数据
LambdaQueryWrapper<主表实体类> wrapper = new LambdaQueryWrapper<>();
wrapper.eq(主表实体类::作为条件的字段getter方法, 作为条件的参数);
主表实体类 master = 主表名Mapper.selectOne(wrapper);

// 步骤二：判空，如果主表数据为空，就没有往下查的必要了
if (Objects.isNull(master)) {
    return null;
}

// 步骤三：selectOne查询分类表数据
LambdaQueryWrapper<从表实体类> wrapper1 = new LambdaQueryWrapper<>();
wrapper1.eq(从表实体类::作为条件的字段getter方法, 作为条件的参数);
从表实体类 slave = 从表名Mapper.selectOne(wrapper1);

// 步骤四：创建vo，准备组装两张表数据
Demo003Vo vo = new Demo003Vo();

// 步骤五：使用copyProperties方法，将goods对象数据复制到vo中
BeanUtils.copyProperties(master, vo);

// 步骤六：使用copyProperties方法，将category对象数据复制到vo中
// 记得判空，因为如果category为空，调用copyProperties会报空指针
if (Objects.nonNull(slave)) {
    BeanUtils.copyProperties(slave, vo);
}

// 返回结果
return vo;
```

### 一主多从
主表一条数据，对应多条从表数据

举例：查询一条商品信息，以及对应的评论列表，组装后返回

返回字段如下：
* goods_id     商品id，来源于商品表
* goods_name   商品名，来源于商品表
* price        商品金额，来源于商品表
* commentList  评论列表，来源于评论表

查看：com.itheima.service.MyBatisPlusService.demo004(String goodsId);

### 多主多从

主表多条数据，每条主表数据对应一条从表数据
举例：查询商品列表，列表中每条数据包含一个分类名称字段，这个字段要从分类表中查询

查看：com.itheima.service.MyBatisPlusService.demo005(String cateId);