package com.itheima.domain;

import lombok.Data;

/**
 * @author itheima
 * @since 2022-02-09
 */
@Data
public class Category {
    private String cateId;
    private String cateName;
}
