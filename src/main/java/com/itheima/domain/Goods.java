package com.itheima.domain;

import lombok.Data;

/**
 * @author itheima
 * @since 2022-02-09
 */
@Data
public class Goods {
    private String goodsId;
    private String cateId;
    private String goodsName;
    private Integer price;
}
