package com.itheima.domain;

import lombok.Data;

/**
 * @author itheima
 * @since 2022-02-09
 */
@Data
public class Comment {
    private String commentId;
    private String goodsId;
    private String content;
}
