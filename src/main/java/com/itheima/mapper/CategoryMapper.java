package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.domain.Category;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author itheima
 * @since 2022-02-09
 */
@Repository
@Mapper
public interface CategoryMapper extends BaseMapper<Category> {
}
