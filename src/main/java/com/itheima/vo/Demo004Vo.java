package com.itheima.vo;

import com.itheima.domain.Comment;
import lombok.Data;

import java.util.List;

/**
 * @author itheima
 * @since 2022-02-09
 */
@Data
public class Demo004Vo {
    private String goodsId;
    private String goodsName;
    private Integer price;
    private List<Comment> commentList;
}
