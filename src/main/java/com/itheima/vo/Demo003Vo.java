package com.itheima.vo;

import lombok.Data;

/**
 * @author itheima
 * @since 2022-02-09
 */
@Data
public class Demo003Vo {
    private String goodsId;
    private String goodsName;
    private String cateId;
    private String cateName;
    private Integer price;
}
