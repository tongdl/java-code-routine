package com.itheima.vo;

import lombok.Data;

/**
 * @author itheima
 * @since 2022-02-09
 */
@Data
public class Demo005Vo {
    private String goodsId;
    private String goodsName;
    private Integer price;
    private String categoryName;
}
